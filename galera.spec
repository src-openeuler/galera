Name:           galera
Version:        26.4.22
Release:        1
Summary:        Synchronous multi-master replication library
License:        GPL-2.0-only
URL:            https://galeracluster.com/
Source0:        https://releases.galeracluster.com/%{name}-4.22/source/%{name}-4-%{version}.tar.gz

Patch0:         cmake_paths.patch
Patch1:         docs.patch
Patch2:         network.patch
Patch3:         fix-garb-service.patch
Patch4:         galera-4-26.4.22-port-to-newer-cmake.patch

BuildRequires:  asio-devel boost-devel check-devel gcc-c++ openssl-devel cmake systemd
Requires(pre):  /usr/sbin/useradd
Requires:       nmap-ncat
Requires:       procps-ng

%{?systemd_requires}

%description
This is Galera replication - Codership's implementation of the write set replication (wsrep) interface.

%prep
%autosetup -n %{name}-4-%{version} -p1

%build
%cmake \
       -DCMAKE_BUILD_TYPE="%{?with_debug:Debug}%{!?with_debug:RelWithDebInfo}" \
       -DINSTALL_LAYOUT=RPM \
       -DCMAKE_RULE_MESSAGES:BOOL=OFF \
       \
       -DBUILD_SHARED_LIBS:BOOL=OFF \
       \
       -DINSTALL_DOCDIR="share/doc/%{name}/" \
       -DINSTALL_GARBD="sbin" \
       -DINSTALL_GARBD-SYSTEMD="sbin" \
       -DINSTALL_CONFIGURATION="/etc/sysconfig/" \
       -DINSTALL_SYSTEMD_SERVICE="lib/systemd/system" \
       -DINSTALL_LIBDIR="%{_lib}/galera" \
       -DINSTALL_MANPAGE="share/man/man8"

cmake -B %_vpath_builddir -LAH

%cmake_build

%install
%cmake_install

mv %{buildroot}%{_unitdir}/garb.service %{buildroot}%{_unitdir}/garbd.service

%check
%ctest

%post
%systemd_post garbd.service

%preun
%systemd_preun garbd.service

%postun
%systemd_postun_with_restart garbd.service

%files
%config(noreplace,missingok) %{_sysconfdir}/sysconfig/garb
%dir %{_docdir}/galera
%doc %{_docdir}/galera/*
%dir %{_libdir}/galera
%{_libdir}/galera/libgalera_smm.so
%{_sbindir}/garbd*
%{_unitdir}/garbd.service

%attr(755, -, -) %{_sbindir}/garb-systemd
%{_mandir}/man8/garbd.8*

%changelog
* Mon Mar 03 2025 Funda Wang <fundawang@yeah.net> - 26.4.22-1
- update to 26.4.22

* Thu Jan 02 2025 wangkai <13474090681@163.com> -  26.4.21-1
- Upgrade to 26.4.21
- Switch scons build to cmake build

* Wed Dec 06 2023 yaoxin <yao_xin001@hoperun.com> - 26.4.16-1
- Upgrade to 26.4.16 and fix build error

* Tue Oct 31 2023 wangkai <13474090681@163.com> - 26.4.14-1
- Update to 26.4.14

* Thu Dec 22 2022 wanglin <wangl29@chinatelecom.cn> - 26.4.8-2
- Set strict_build_flags=0 and fix changelog date

* Wed Aug 25 2021 lingsheng <lingsheng@huawei.com> - 26.4.8-1
- Update to 26.4.8

* Mon Aug 16 2021 lingsheng <lingsheng@huawei.com> - 25.3.26-5
- Remove unsupported reload option

* Wed Jul 21 2021 lingsheng <lingsheng@huawei.com> - 25.3.26-4
- Remove unnecessary buildrequire gdb

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 25.3.26-3
- add gdb in buildrequires

* Wed Mar 4 2020 zhouyihang<zhouyihang1@huawei.com> - 25.3.26-2
- Pakcage init
